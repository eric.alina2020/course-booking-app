// DEPENDENCIES AND MODULES
	const mongoose = require("mongoose");

// SCHEMA / DOCUMENT BLUEPRINT
	const courseSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, 'Course Name is Required']
		},
		description: {
			type: String,
			required: [true, 'Course Description is Required']
		},
		price: {
			type: Number,
			required: [true, 'Course Price is Required']
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		},
		enrollees: [
			{
				userId: {
					type: String,
					required: [true, "User's ID is Required"]
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				}
			}
		]
	});

// MODEL
	const Course = mongoose.model("Course", courseSchema);
	module.exports = Course;
