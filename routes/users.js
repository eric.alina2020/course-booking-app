// DEPENDENCIES AND MODULES
	const exp = require("express");
	const controller = require('./../controllers/users.js');
	const auth = require("../auth");

	const {verify, verifyAdmin} = auth;

// ROUTING COMPONENTS
	const route = exp.Router();


// [POST] ROUTES
	route.post('/register', (req, res) => {
		let userDetails = req.body;
		controller.registerUser(userDetails).then(outcome => {
			res.send(outcome);
		});
	});

/*	route.post('/login', (req, res) => {
		//console.log(req.body); //displays the input from the user in CLI.
		let userDetails = req.body;
		controller.loginUser(userDetails).then(outcome => {
			// Send the outcome back to the client.
			res.send(outcome);
		});
	});
*/

	// LOGIN
	route.post("/login", controller.loginUser);

	// ENROLL OUR REGISTERED USERS
	route.post('/enroll', verify, controller.enroll);

// [GET] ROUTES

	// GET USER DETAILS
	route.get("/getUserDetails", verify, controller.getUserDetails);

	// GET LOGGED USER'S ENROLLMENTS
	route.get('/getEnrollments', verify, controller.getEnrollments);
	
// [PUT] ROUTES

// [DELETE] ROUTES

// EXPOSE ROUTE SYSTEM
	module.exports = route;