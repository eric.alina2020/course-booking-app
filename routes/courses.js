// DEPENDENCIES AND MODULES
	const exp = require("express");
	const controller = require('../controllers/courses');
	const auth = require("../auth");
	const {verify, verifyAdmin} = auth;

// ROUTING COMPONENT
	const route = exp.Router();


// [POST] ROUTES
	route.post('/create', verify, verifyAdmin, (req, res) => {
		let data = req.body;
		controller.createCourse(data).then(outcome => {
			res.send(outcome);
		});
	});


// [GET] ROUTES
	route.get('/all', verify, verifyAdmin, (req, res) => {
		controller.getAllCourse().then(outcome => {
			res.send(outcome);
		});
	});

	route.get('/:id', (req, res) => {
		let courseId = req.params.id;
		controller.getCourse(courseId).then(result => {
			res.send(result);
		});
	});

	route.get('/', (req, res) => {
		controller.getAllActiveCourse().then(outcome => {
			res.send(outcome);
		});
	});


// [PUT] ROUTES
	route.put('/:id', verify, verifyAdmin, (req, res) => {
		let id = req.params.id;
		let details = req.body;
		let cName = details.name;
		let cDesc = details.description;
		let cCost = details.price;

		if (cName !== '' && cDesc !== '' && cCost !== '') {
			controller.updateCourse(id, details).then(outcome => {
				res.send(outcome);
			});
		} else {
			res.send('Incorrect Input. Make sure that the details are complete.');
		};
	});

	route.put('/:id/archive', verify, verifyAdmin, (req, res) => {
		let id = req.params.id;
		controller.deactivateCourse(id).then(result => {
			res.send(result);
		});
	});

	route.put('/:id/reactivate', verify, verifyAdmin, (req, res) => {
		let id = req.params.id;
		controller.reactivateCourse(id).then(outcome => {
			res.send(outcome);
		});
	});

// [DELETE] ROUTES
	route.delete('/:id', verify, verifyAdmin, (req, res) => {
		let id = req.params.id;
		controller.deleteCourse(id).then(outcome => {
			res.send(outcome);
		});
	});

// EXPORT ROUTE SYSTEM
	module.exports = route;